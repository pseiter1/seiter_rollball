﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playercontroller : MonoBehaviour {

    public float speed;

    public Text countText;

    private Rigidbody rb;

    public LayerMask groundLayers;

    public float jumpForce = 7;

    public SphereCollider col;

    private int count;

    public Text winText;

    public GameObject player;

    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText ();
        winText.text = "";
        col = GetComponent<SphereCollider>();
    }
    [System.Obsolete]
    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

        rb.AddForce (movement * speed);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("pick up"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText ();
        }


    }
    [System.Obsolete]
    void OnCollisionEnter(Collision other)
    {
 
        
        if (other.gameObject.CompareTag("Enemy"))
        {
            Application.LoadLevel(Application.loadedLevel);

        }

    }

    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString ();
        if (count >= 11)
        {
            winText.text = "you win!";
        }
    }

    
}

