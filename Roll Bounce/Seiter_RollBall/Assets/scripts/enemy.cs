﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour
{
    public GameObject Player;
    public int MoveSpeed = 2;
    public int MaxDist = 10;
    public int MinDist = 0;





    void Start()
    {

    }

    void Update()
    {
        transform.LookAt(Player.transform);

        if (Vector3.Distance(transform.position, Player.transform.position) >= MinDist)
        {

            transform.position += transform.forward * MoveSpeed * Time.deltaTime;


            if (Vector3.Distance(transform.position, Player.transform.position) <= MaxDist)
            {
                return; 
            }

        }
    }
}